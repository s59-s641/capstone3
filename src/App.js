
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';





// components
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';

import './App.css';

function App() {
  return (
   <Router>  
       <AppNavbar/>
       <Container className="mx-auto">
           <Routes>
               <Route path="/" element={<Home/>} />
        {/*       <Route path="/courses" element={<Courses/>} />
               <Route path="/courses/:courseId" element={<CourseView/>} />*/}
               <Route path="/login" element={<Login/>} />
               {/*<Route path="/logout" element={<Logout />} />*/}
               <Route path="/register" element={<Register/>} /> 
               {/*<Route path="*" element={<Error />} />*/}
           </Routes>
       </Container>       
   </Router>
  );
}

export default App;
