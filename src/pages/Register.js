import {useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate , useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
// import UserContext from '../UserContext';


export default function Register() {

// const {user, setUser} = useContext(UserContext);

const navigate = useNavigate();
	
const [firstName, setfirstName] = useState('');
const [lastName, setlastName] = useState('');
const [email, setEmail] = useState('');
const [mobileNo, setMobileNo] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
const [isActive, setIsActive] = useState(false);


function registration(e) {
	e.preventDefault();
	
	//verify email
		Swal.fire({
 					title:"Registration Succesfull",
 					icon: "success",
 					text:"Your registration is Succes!"
 				})

				setfirstName("");
				setlastName("");
				setEmail("");
				setPassword1("");
				setPassword2("");

				navigate("/login");
				
	
};

 	useEffect(() => {
 		if((firstName!== "" &&
 			lastName.trim() !== "" &&
 			mobileNo.length === 11 &&
 			email !== "" &&
 			password1 !== "" &&
 			password2 !== "" 
 			) && (password1 === password2)){
 			setIsActive(true)
 		}else{
 			setIsActive(false)
 		}
 	},[firstName,lastName,email,mobileNo,password1,password2]);


    return (

        <Form className="mt-5 border shadow p-3 rounded" onSubmit= {(e)=> registration(e)}>

        <Form.Group className="mb-3" controlId="firstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control 
	        	type="text" 
	        	value ={firstName}
	        	onChange={(e)=> setfirstName(e.target.value.trim() )}
	        	placeholder="Enter Your First Name"
	        	required
	        	/>
	    
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="lastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control 
	        	type="Text" 
	        	value ={lastName}
	        	onChange={(e)=> setlastName(e.target.value.trim() )} 
	        	placeholder="Enter Your Last Name"

	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	value ={email}
	        	onChange={(e)=> setEmail(e.target.value.replace(/\s/g, ""))} 
	        	placeholder="Enter Your Email"
	        	autoComplete="off"
	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="mobileNo">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control 
	        	type="Text" 
	        	value ={mobileNo}
	        	onChange={(e)=> setMobileNo(e.target.value.replace(/\s/g, ""))} 
	        	placeholder="09876543211"
	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	value={password1}
	        	onChange={(e)=> setPassword1(e.target.value.replace(/\s/g, "")) }
	        	placeholder="Enter Your Password"
	        	 autoComplete="new-password"
	        	required
	        	 />
	      </Form.Group>
	  		
	      <Form.Group className="mb-3" controlId="password2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control  
	        type="password" 
	        value={password2}
	        onChange={(e)=> setPassword2(e.target.value) }
	        placeholder="Verify Your Password"
	        autoComplete="new-password" 
	        required
	        />
	      </Form.Group>
	     	{ isActive ?
	     		 <Button variant="primary" type="submit" id="submitBtn">
	       		 Submit
	     		 </Button>
	     		 :
	     		 <Button variant="primary" type="submit" id="submitBtn" disabled>
	       		 Submit
	     		 </Button>
	     	}
	    </Form>

    );
}