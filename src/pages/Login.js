import {useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

export default function Login() {

	// const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

 
 	function  loginUser(e){
 		e.preventDefault();

 			Swal.fire({
 					title:"Login Succesfull",
 					icon: "success",
 					text:"You have Login Succesfully!"
 				})
 			// localStorage.setItem("email",email);
 			// setUser({email: localStorage.getItem('email')})
 			setEmail("");
 			setPassword("");
 			// console.log(`${email} has been verified!`)
 			// alert('User Succesfully log in!');
 		
 		
 	}

 	useEffect(() => {
 		if(email !== "" && password !== "" ) {
 			setIsActive(true)
 		}else{
 			setIsActive(false)
 		}
 	},[email,password]);

    return (

    	// (user.id !== null) ?
    	// 	<Navigate to="/courses" />
    	// :

        <Form className="mt-5 border shadow p-3 rounded" onSubmit= {(e)=> loginUser(e)}>
	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	value ={email}
	        	onChange={(e)=> setEmail(e.target.value.replace(/\s/g, ""))} 
	        	placeholder="Enter Your Email"
	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	value={password}
	        	onChange={(e)=> setPassword(e.target.value.replace(/\s/g, "")) }
	        	placeholder="Enter Your Password"
	        	required 
	        	autoComplete="off"
	        	 />
	        	
	      </Form.Group>
	     	{ isActive ?
	     		 <Button variant="success" type="submit" id="submitBtn">
	       		 Submit
	     		 </Button>
	     		 :
	     		 <Button variant="success" type="submit" id="submitBtn" disabled>
	       		 Submit
	     		 </Button>
	     	}
	    </Form>

    );
}